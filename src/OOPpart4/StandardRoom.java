package OOPpart4;

public class StandardRoom extends Room {

    public StandardRoom(int roomNumber,double nightlyRate){
        super(roomNumber,nightlyRate);
    }

    @Override
    public void book() {
        if(!isBooked){
            isBooked = true;
            System.out.println("Standard room "+ roomNumber + " has been successfully booked.");
        }else{
            System.out.println("Standard room "+ roomNumber + " is already booked!");
        }
    }

    @Override
    public void checkAvailability() {
        if(isBooked){
            System.out.println("Standard Room " + roomNumber + " is not available.");
        }else {
            System.out.println("Standard Room " + roomNumber + " is available.");
        }
    }
}
