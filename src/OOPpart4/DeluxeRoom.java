package OOPpart4;

public class DeluxeRoom extends Room{
    private int numBeds;

    public DeluxeRoom(int roomNumber,double nightlyRate,int numBeds){
        super(roomNumber,nightlyRate);
        this.numBeds = numBeds;
    }


    @Override
    public void book() {
        if(!isBooked){
            isBooked = true;
            System.out.println("Deluxe room "+ roomNumber + "with "+ this.numBeds+" beds"+" has been successfully booked.");
        }else{
            System.out.println("Deluxe room "+ roomNumber + " is already booked!");
        }
    }

    @Override
    public void checkAvailability() {
        if(isBooked){
            System.out.println("Deluxe Room " + roomNumber + " is not available.");
        }else {
            System.out.println("Deluxe Room " + roomNumber + " is available.");
        }
    }
}
