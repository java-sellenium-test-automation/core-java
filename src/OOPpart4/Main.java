package OOPpart4;

public class Main {
    public static void main(String[] args) {
        Room standardRoom = new StandardRoom(101, 100);
        Room deluxeRoom = new DeluxeRoom(201, 150, 2);
        Room suite = new Suite(301, 200, true);

        standardRoom.checkAvailability();
        standardRoom.book();
        standardRoom.checkAvailability();

        deluxeRoom.checkAvailability();
        deluxeRoom.book();
        deluxeRoom.checkAvailability();

        suite.checkAvailability();
        suite.book();
        suite.checkAvailability();

        System.out.println(standardRoom.calculateCharges(3));
        System.out.println(deluxeRoom.calculateCharges(5));
        System.out.println(suite.calculateCharges(2));
    }
}
