package OOPpart4;

public class Suite extends Room {
    private boolean hasLivingRoom;

    public Suite(int roomNumber, double nightlyRate,boolean hasLivingRoom){
        super(roomNumber,nightlyRate);
        this.hasLivingRoom = hasLivingRoom;
    }

    @Override
    public void book() {
        if (!isBooked) {
            isBooked = true;
            System.out.println("Suite " + roomNumber + (hasLivingRoom ? " with living room": " without living room")+" has been booked.");
        } else {
            System.out.println("Suite " + roomNumber + " is already booked.");
        }
    }

    @Override
    public void checkAvailability() {
        if (isBooked) {
            System.out.println("Suite " + roomNumber + " is not available.");
        } else {
            System.out.println("Suite " + roomNumber + " is available.");
        }
    }
}
