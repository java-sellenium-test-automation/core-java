package OOPpart4;

public abstract class Room {

    protected int roomNumber;
    protected double nightlyRate;
    protected boolean isBooked;

    public Room(int roomNumber, double nightlyRate) {
        this.roomNumber = roomNumber;
        this.nightlyRate =nightlyRate;
        this.isBooked = false;
    }

    public abstract void book();
    public abstract  void checkAvailability();

    public double calculateCharges(int numNights){
      return nightlyRate*numNights;
    }
}
