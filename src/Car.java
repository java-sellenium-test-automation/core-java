
    // Init car class
    public class Car{
        public String brand;
        public String color;
        public int year;

        public Car(String brand,String color,int year){
            this.brand =brand;
            this.year = year;
            this.color = color;
        }

        public void startEngine(){
            System.out.println("Engine is starting");
        }

        public void drive(){
            System.out.println("Car is moving...");
        }



    }