package Student;

public class Student {

    private String name;

    private int id;


    private int age;


    public Student(String name, int id, int age) {
        this.name = name;
        this.id = id;
        this.age = age;
    }

    public int getId(){ return id;}

    public String getName(){ return  name;}

    public int getAge(){return age;}
}
