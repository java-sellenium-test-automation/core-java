package Student;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.SortedMap;

public class StudentDatabase {

    private  ArrayList<Student> students;

    public StudentDatabase() { students = new ArrayList<>();}

    public void addStudent(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter student's name:");
        String name = sc.nextLine();
        System.out.println("Enter student's ID:");
        int id = sc.nextInt();
        System.out.println("Enter student's Age:");
        int age = sc.nextInt();
        sc.nextLine();


        Student student = new Student(name,id,age);
        students.add(student);
        System.out.println("Students added to the database");
    }

    public void deleteStudent(int id){

        for (Student student : students){
            if (student.getId() == id){
                students.remove(student);
                System.out.println("Student with id "+id+" has been removed from DB");
                return;
            }
        }
        System.out.println("Student with id "+id+" not found!");
    }

    public void searchStudent(int id){
        for(Student student : students){
            if(student.getId() == id){
                System.out.println("The student is found!. Name is : "+student.getName());
                return;
            }
        }
        System.out.println("Student with id "+id+" not found!");
    }






}
