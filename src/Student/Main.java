//package Student;
//
//import Book.BookInventory;
//
//import java.util.Scanner;
//
//public class Main {
//
//    public static void main(String[] args) {
//        BookInventory bookInventory = new BookInventory();
//
//        Scanner sc =  new Scanner(System.in);
//
//        while (true){
//            String title = "";
//            System.out.println("Select option from the menu by number");
//            System.out.println("1. Add a book");
//            System.out.println("2. Remove a book");
//            System.out.println("3. Search for a book");
//            System.out.println("4. Display all books");
//            System.out.println("5. Exit");
//
//            int select = sc.nextInt();
//            sc.nextLine();
//
//            switch (select) {
//                case  1:
//                    bookInventory.addBook();
//                    break;
//                case 2:
//                    System.out.println("Enter book title to delete a book: ");
//                    title = sc.nextLine();
//                    bookInventory.removeBook(title);
//                    break;
//                case 3:
//                    System.out.println("Enter book title to search for a book: ");
//                    title = sc.nextLine();
//                    bookInventory.searchBook(title);
//                    break;
//                case 4:
//                    System.out.println("Displaying all books....");
//                    bookInventory.displayAllBooks();
//                    break;
//                case 5:
//                    System.out.println("Closing the inventory program...");
//                    System.exit(0);
//                    break;
//                default:
//                    System.out.println("Invalid option please select options from the menu");
//                    break;
//            }
//        }
//    }
//}
