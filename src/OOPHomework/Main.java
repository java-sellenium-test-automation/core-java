package OOPHomework;

public class Main {

    public static void main(String[] args) {

        //Task1 Abstract Class
        Vehicle car = new Car();
        car.startEngine();
        car.drive();


        Vehicle moto = new Motorcycle();
        moto.startEngine();
        moto.drive();

        Vehicle truck = new Truck();
        truck.startEngine();
        truck.drive();

//
//        //Task2 Getter and Setter
//        Student student = new Student();
//
//        student.setName("Santa");
//        student.setAge(22);
//        student.setGrade(98.2f);
//
//        System.out.println(student.getName());
//        System.out.println(student.getAge());
//        System.out.println(student.getGrade());


    }



}
