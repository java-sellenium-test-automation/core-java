package OOPHomework;

public class Motorcycle extends Vehicle{
    @Override
    public void startEngine() {
        System.out.println("Motorcycle engine has started....");
    }

    @Override
    public void drive() {
        System.out.println("Motorcycle started to move....");
    }
}
