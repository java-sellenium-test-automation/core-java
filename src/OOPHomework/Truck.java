package OOPHomework;

public class Truck extends Vehicle {


    @Override
    public void startEngine() {
        System.out.println("Truck engine has started....");
    }

    @Override
    public void drive() {
        System.out.println("Truck started to move.....");
    }
}
