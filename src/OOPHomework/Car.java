package OOPHomework;

public class Car extends Vehicle {
    @Override
    public void startEngine() {
        System.out.println("Car engine has started....");
    }

    @Override
    public void drive() {
        System.out.println("Car started to move....");
    }
}
