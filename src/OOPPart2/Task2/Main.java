package OOPPart2.Task2;

public class Main {
    public static void main(String[] args) {

        //Create instances
        Student student = new Student();
        Teacher teacher = new Teacher();

        //Set student details
        student.setName("Mehdi");
        student.setAge(28);
        student.setId(2124565);

        //Set teacher details
        teacher.setName("Vugar");
        teacher.setAge(33);
        teacher.setClassName("QA Automation");


        //Display student and teacher info
        student.displayInfo();
        teacher.displayInfo();









    }
}
