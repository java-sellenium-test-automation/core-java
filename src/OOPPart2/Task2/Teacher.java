package OOPPart2.Task2;

public class Teacher extends Person {




    String className;
    @Override
    public void displayInfo() {
        System.out.println("The teacher name is "+ this.getName() +" and age is "+this.getAge()+". Teacher is teaching "+className+".");
    }

    public void setClassName(String className) {
        this.className =className;
    }
}
