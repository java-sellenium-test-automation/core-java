package OOPPart2.Task2;

public class Student extends Person{


    private int id;
    @Override
    public void displayInfo() {
        System.out.println("The student name is "+ this.getName() +" and age is "+this.getAge()+". Student's ID is: "+id);
    }

    public void setId(int id) {
        this.id = id;
    }

}
