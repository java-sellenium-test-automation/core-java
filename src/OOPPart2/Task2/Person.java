package OOPPart2.Task2;

public abstract class Person {

    private String name;

    private int age;

    public abstract void displayInfo();

    public String getName(){
      return name;
    };

    public int getAge(){
        return age;
    }

    public void setName(String name){this.name = name;}

    public void setAge(int age) {this.age = age;}
}
