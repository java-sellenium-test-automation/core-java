package OOPPart2.Task1;

import OOPPart2.Task1.Animal;

public class Cat extends Animal {

    @Override
    public void makeSound() {
        System.out.println("Meow");
    }
}
