package OOPPart2.Task1;

public class Main {

    public static void main(String[] args) {

        Cow cow = new Cow();
        Dog dog = new Dog();
        Cat cat = new Cat();

        cat.makeSound();
        cow.makeSound();
        dog.makeSound();

    }
}
