package OOPPart2.Task3;

public class Music extends Media{

    @Override
    public void play() {
        System.out.println("Now playing music..." + this.getTitle()+" . Duration is "+this.getDuration()+" minutes.");
    }
}
