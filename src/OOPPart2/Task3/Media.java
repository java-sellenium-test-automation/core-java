package OOPPart2.Task3;

public abstract class Media {

    private String title;

    private double duration;


    public void setTitle(String title) {
        this.title = title;
    }

    public void setDuration(double duration) {
        this.duration = duration;
    }

    public double getDuration() {
        return duration;
    }

    public String getTitle() {
        return title;
    }


    public abstract void play();
}
