package OOPPart2.Task3;

public class Main {

    public static void main(String[] args) {

        //Create instances
        Movie movie = new Movie();
        Music music = new Music();


        //Set music and Movie details
        music.setTitle("Sting -  Shape of my heart");
        music.setDuration(3.25);

        movie.setTitle("Interstellar");
        movie.setDuration(138);


        //Play media
        movie.play();
        music.play();

    }

}
