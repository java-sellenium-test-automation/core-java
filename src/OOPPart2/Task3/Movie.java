package OOPPart2.Task3;

public class Movie extends Media {

    @Override
    public void play() {
        System.out.println("Now playing ..." + this.getTitle()+" movie. Duration is "+this.getDuration()+" minutes.");
    }
}
