package OOPPart2.Task4;

public interface Logger {

    public void logWarning(String message);

    abstract void logInfo(String message);

    public void logError(String message);


}
