package OOPPart2.Task4;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class FileLogger implements Logger{

    private String fileName =  "logs.txt";


    public void fileWriter(String message){
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName,true))) {
            writer.write(message);
            writer.newLine();
        } catch (IOException e) {
            System.out.println("An error occurred while writing to the file: " + e.getMessage());
        }
    }

    @Override
    public void logInfo(String message) {
        fileWriter("Info message:\n"+message);
    }

    @Override
    public void logWarning(String message) {
        fileWriter("Warning message:\n"+message);
    }

    @Override
    public void logError(String message) {
        fileWriter("Error message:\n"+message);
    }
}
