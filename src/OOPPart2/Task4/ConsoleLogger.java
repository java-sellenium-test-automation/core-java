package OOPPart2.Task4;

import java.util.Scanner;

public class ConsoleLogger implements Logger{


    @Override
    public void logInfo(String message) {
        System.out.println("Info message:\n"+message);
    }

    @Override
    public void logError(String message) {
        System.out.println("Error message:\n"+message);
    }

    @Override
    public void logWarning(String message) {
        System.out.println("Warning message:\n"+message);
    }
}
