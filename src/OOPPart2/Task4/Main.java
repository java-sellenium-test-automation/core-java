package OOPPart2.Task4;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        //Create scanner instance
        Scanner sc = new Scanner(System.in);

    //Console Logger
        //Create instance
        ConsoleLogger consoleLogger = new ConsoleLogger();


        //Get user input
        System.out.println("Enter console log info message:");
        String infoMessage = sc.nextLine();
        System.out.println("Enter console log warning message:");
        String warningMessage = sc.nextLine();
        System.out.println("Enter console log error message:");
        String errorMessage = sc.nextLine();

        // Display message on the console
        consoleLogger.logInfo(infoMessage);
        consoleLogger.logWarning(warningMessage);
        consoleLogger.logError(errorMessage);

    //FileLogger

        //Create instance
        FileLogger fileLogger = new FileLogger();

        //Get user input
        System.out.println("Enter log info message:");
        infoMessage = sc.nextLine();
        System.out.println("Enter log warning message:");
        warningMessage = sc.nextLine();
        System.out.println("Enter log error message:");
        errorMessage = sc.nextLine();


        // Write messages to the file.

        fileLogger.logInfo(infoMessage);
        fileLogger.logWarning(warningMessage);
        fileLogger.logError(errorMessage);
        System.out.println("File successfully written.");








    }
}
