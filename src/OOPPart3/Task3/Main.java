package OOPPart3.Task3;

public class Main {
    public static void main(String[] args) {
            AudioPlayer audioPlayer = new AudioPlayer();
            audioPlayer.play();
            audioPlayer.stop();

            VideoPlayer videoPlayer = new VideoPlayer();
            videoPlayer.play();
            videoPlayer.stop();

    }
}
