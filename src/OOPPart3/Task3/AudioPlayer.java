package OOPPart3.Task3;

public class AudioPlayer implements Playable{
    @Override
    public void play() {
        System.out.println("Audio playback started");
    }

    @Override
    public void stop() {
        System.out.println("Audio playback stopped");
    }
}
