package OOPPart3.Task3;

public interface Playable {

    public void play();

    public void stop();

}
