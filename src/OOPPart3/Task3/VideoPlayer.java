package OOPPart3.Task3;

public class VideoPlayer implements Playable{
    @Override
    public void play() {
        System.out.println("Video playback started");
    }

    @Override
    public void stop() {
        System.out.println("Video playback stopped");
    }
}
