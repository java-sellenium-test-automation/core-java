package OOPPart3.Task4;

public abstract class Car implements Rentable {
    //Properties
    private String make;
    private String model;
    private int year;
    private double rentalRate;
    private boolean available;

    public Car(String make, String model, int year, double rentalRate) {
        this.make = make;
        this.model = model;
        this.year = year;
        this.rentalRate = rentalRate;
        this.available = true;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getRentalRate() {
        return rentalRate;
    }

    public void setRentalRate(double rentalRate) {
        this.rentalRate = rentalRate;
    }

    public boolean isAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public abstract double calculateRentalCharge(int numDays);

    public void displayCarInfo() {
        System.out.println("Make: " + make);
        System.out.println("Model: " + model);
        System.out.println("Year: " + year);
        System.out.println("Rental Rate: " + rentalRate);
    }

    @Override
    public void rent(int numDays) {
        if (available) {
            available = false;
            System.out.println("Car rented for " + numDays + " days");
        } else {
            System.out.println("Car is not available for rent");
        }
    }

    @Override
    public void returnCar() {
        if (!available) {
            available = true;
            System.out.println("Car returned");
        } else {
            System.out.println("Car is already available");
        }
    }
}
