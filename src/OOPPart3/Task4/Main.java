package OOPPart3.Task4;

public class Main {
    public static void main(String[] args) {
        EconomyCar economyCar = new EconomyCar("Toyota", "Corolla", 2020, 50, 30);
        LuxuryCar luxuryCar = new LuxuryCar("Mercedes", "S-Class", 2021, 200, new String[]{"Leather Seats", "Sunroof","HeadUp Display","4 zone A/C"});

        RentalTransaction rentalTransaction1 = new RentalTransaction(economyCar, "Akif Aliyev", 5);
        rentalTransaction1.displayTransactionInfo();

        RentalTransaction rentalTransaction2 = new RentalTransaction(luxuryCar, "Mammad Mammadzada", 3);
        rentalTransaction2.displayTransactionInfo();

        economyCar.rent(7);
        economyCar.returnCar();
    }
}
