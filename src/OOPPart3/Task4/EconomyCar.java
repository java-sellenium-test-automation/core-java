package OOPPart3.Task4;

public class EconomyCar extends Car{
    private double fuelEfficiency;

    public double getFuelEfficiency() {
        return fuelEfficiency;
    }

    public void setFuelEfficiency(double fuelEfficiency) {
        this.fuelEfficiency = fuelEfficiency;
    }

    public EconomyCar(String make, String model, int year, double rentalRate, double fuelEfficiency) {
        super(make, model, year, rentalRate);
        this.fuelEfficiency = fuelEfficiency;
    }

    @Override
    public double calculateRentalCharge(int numDays) {
        return getRentalRate() * numDays;
    }


}
