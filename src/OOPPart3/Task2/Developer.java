package OOPPart3.Task2;

public class Developer extends Employee{
    private String programmingLang;


    public Developer(String name, double salary ,String programmingLang){
        super(name,salary);
        this.programmingLang = programmingLang;
    }

    @Override
    public void getDetails() {
        System.out.println("Name: " + getName());
        System.out.println("Salary: " + getSalary());
        System.out.println("Programming Language: " + programmingLang);
    }

}
