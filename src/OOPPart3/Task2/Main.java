package OOPPart3.Task2;

public class Main {
    public static void main(String[] args) {
        Manager manager = new Manager("Alice", 5000, "Sales");
        manager.getDetails();

        Developer developer = new Developer("Jane", 4000, "Java");
        developer.getDetails();
    }

}