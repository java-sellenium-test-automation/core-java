package OOPPart3.Task2;

public abstract class Employee {
    private String name;

    private double salary;

    public Employee(String name, double salary) {
        this.name = name;
        this.salary = salary;
    }

    public abstract void getDetails();

    public String getName() {
        return name;
    }

    public double getSalary() {
        return salary;
    }
}
