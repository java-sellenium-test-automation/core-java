package OOPPart3.Task1;

public class Main {

    public static void main(String[] args) {
        Car car =  new Car();


        car.setMake("Toyota");
        car.setModel("Yaris");
        car.setYear(2021);
        car.setRentalPrice(65.7);


        System.out.println("The rental price for "+car.getMake()+" "+car.getModel()+" "+car.getYear()+" is: "+car.getRentalPrice()+" $/Day");
    }
}
