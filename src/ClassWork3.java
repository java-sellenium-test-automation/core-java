public class ClassWork3 {

    public static void main(String[] args) {


        Cat kitty = new Cat("Kitty","Brown",2);
        Cat mitty = new Cat("Mitty","White",1);

        System.out.println(kitty.sleep());
        System.out.println(kitty.eat());
        System.out.println(kitty.play());

        System.out.println(mitty.sleep());


    }

    private static class  Cat{

        String name;

        int age;

        String color;


        private String eat(){
            return  this.name+" is eating";
        }

        private String sleep(){
            return  this.name+" is sleeping";
        }

        private String play(){
            return  this.name+" is playing";
        }

        private Cat(String name , String color,int age) {
            this.name = name;
            this.color = color;
            this.age =age;
        }
        private Cat(String name ,int age) {
            this.name = name;
            this.age =age;
        }



    }

}
