import java.util.Arrays;
import java.util.Scanner;

public class Homework2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a number to calculate it's factorial");
        int n = scanner.nextInt();
        factorialNumber(n);
        System.out.println("Please enter a number to calculate sum of it's digits");
        n = scanner.nextInt();
        sumOfDigits(n);
        System.out.println("Please enter a number to mirror it");
        n = scanner.nextInt();
        mirrorNumber(n);
        int[] array = new int[]{1,3,45,6,543,54,-2,-56,0,12};
        minOfArray(array);
        minIndexOfArray(array);
        sumOfOddElements(array);
        reverseAnArray(array);
        countOfOddElements(array);
        swapHalfOfArray(array);
    }

    public static void factorialNumber(int n){
        int factorial=n;

        if (n == 0 || n == 1){
            factorial = 1;
        }else {
            for (int i = n; i>=2;i--){
                factorial = factorial*(i-1);
            }
        }

        System.out.println("Factorial of "+n+" is equal to "+factorial);

    }

    public static void sumOfDigits(int n){
        int sumOfDigits = 0;
        while (n > 0){
            sumOfDigits += n%10;
            n = n/10;
        }

        System.out.println("Sum of digits of  is equal to "+sumOfDigits);
    }

    public static void mirrorNumber(int n){
        int temp;
        String reversed = "";
        while (n > 0){
            temp = n%10;
            n = n/10;
            reversed += temp;
        }

        System.out.println("Reverse is "+reversed);
    }

    public static void minOfArray(int[] array) {
        int min = array[0];
        for (int number:array){
            if (number<min){
                min = number;
            }
        }
        System.out.println("Minimum of the given array is "+ min);
    }

    public static void minIndexOfArray(int[] array) {

        int index = 0;
        int i = 0;
        int len = array.length;
        while(i < len ){
            if (array[i] < array[index]){
                index = i;
            }
            i++;
        }
        System.out.println("Index of minimum of the given array is "+ index);
    }

    public static void sumOfOddElements(int[] array){
        int sum = 0;
        for (int number : array){
            if(number%2==1){
                sum+=number;
            }
        }
        System.out.println("Sum of the odd numbers in a given array is " + sum);
    }

    public static void reverseAnArray(int[] array){
        int len = array.length;
        int[] reversedArray = new int[len];
        int i= 0;
        while (i < len){
            reversedArray[i] = array[len-(i+1)];
            i++;
        }

        System.out.println("The original array was "+Arrays.toString(array));
        System.out.println("Reversed array is "+Arrays.toString(reversedArray));


    }


    public static void countOfOddElements(int[] array){
        int count = 0;
        for (int number : array){
            if(number%2==1){
                count+=1;
            }
        }
        System.out.println("Count of the odd numbers in a given array is " + count);
    }

    public static void swapHalfOfArray(int[] array){
        int len = array.length;
        int halfLen = len/2;
        System.out.println(halfLen);
        int [] swappedArray = new int[len];
        for (int i=0;i<len;i++){
            if (i<halfLen){
                swappedArray[i+halfLen] = array[i];
            } else if ( i >= halfLen) {
                swappedArray[i-halfLen] = array[i];
            }
        }

        System.out.println("The original array was "+Arrays.toString(array));
        System.out.println("Swapped array is "+Arrays.toString(swappedArray));




    }


}
