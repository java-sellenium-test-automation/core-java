import java.util.Scanner;

public class Homework1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first number:");
        double a = scanner.nextInt();
        System.out.println("Enter operator number: (+ , - , * , / )");
        char operator = scanner.next().charAt(0);
        System.out.println("Enter first number:");
        double b = scanner.nextInt();
        double result = 0;

        switch (operator){
            case '+':
                result = a + b;
                break;
            case '-':
                result = a - b;
                break;
            case '*':
                result = a * b;
                break;
            case '/':
                result = a / b;
                break;
            default:
                System.out.println("Invalid input!");
                break;
        }

        System.out.println( ""+a+" "+operator+" "+b+" is equal to "+ result);

    }
}
