package Collections.InventoryManager;

import java.util.HashMap;

public class Inventory {

    private final HashMap<String,Integer> inventoryMap;

    public Inventory(){
        inventoryMap = new HashMap<>();
    }

    public void addItem(String item, int quantity){
        inventoryMap.put(item,quantity);
    }

    public void updateQuantity(String item,int quantity){
        if (inventoryMap.containsKey(item)) {
            inventoryMap.put(item,quantity);
            System.out.println("Quantity of the "+item+" has been updated");
        }else{
            System.out.println(item+" does not exist in the inventory.");
        }
    }

    public void removeItem(String item){
        if (inventoryMap.containsKey(item)) {
            inventoryMap.remove(item);
            System.out.println(item+" has been removed from the inventory.");
        }else{
            System.out.println(item+" does not exist in the inventory.");
        }
    }

    public boolean itemExists(String item){
        return inventoryMap.containsKey(item);
    }

    public void displayInventory(){
        System.out.println(inventoryMap + "\n");
    }



}
