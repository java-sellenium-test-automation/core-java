package Collections.InventoryManager;

public class InventoryManager {

    public static void main(String[] args) {

        Inventory inventory = new Inventory();

        // Adding items to the inventory
        inventory.addItem("Pencil",20);
        inventory.addItem("Notebook",12);
        inventory.addItem("Scissors",5);

        // Displaying inventory after addition
        System.out.println("The inventory has below items: ");
        inventory.displayInventory();

        // Updating the quantity of an inventory item
        inventory.updateQuantity("Notebook",25);

        // Displaying inventory after update
        System.out.println("The inventory has below items after update: ");
        inventory.displayInventory();

        // Removing an item from the inventory
        inventory.removeItem("Pencil");

        // Displaying inventory after removal
        System.out.println("The inventory has below items after removal: ");
        inventory.displayInventory();

        // Checking if an item exists in the inventory
        System.out.println("Does Notebook exist in the inventory? " + inventory.itemExists("Notebook"));
        System.out.println("Does Scissors exist in the inventory? " + inventory.itemExists("Scissors"));




    }

}
