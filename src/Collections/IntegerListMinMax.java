package Collections;

import java.util.*;

public class IntegerListMinMax {
    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        List<Integer> removed = new ArrayList<>();

        numbers.add(-23);
        numbers.add(15);
        numbers.add(3);
        numbers.add(8);
        numbers.add(1);

        int min = Collections.min(numbers);
        int max = Collections.max(numbers);

        System.out.println("Minimum value: " + min);
        System.out.println("Maximum value: " + max);

        for (Integer number : numbers) {
            if (number % 3 != 0) {
                removed.add(number);
            }
        }

        System.out.println("List after removing elements divisible by 3: " + removed);
    }


}


