package Collections.StudentRecordSystem;

public class Main {
    public static void main(String[] args) {
        StudentRecordSystem recordSystem = new StudentRecordSystem();

        // Creating students
        Student student1 = new Student(1, "Mehdi Nuruzade", 3.8);
        Student student2 = new Student(2, "Ilgar Karimov", 3.9);
        Student student3 = new Student(3, "Aysel Huseynova", 3.7);

        // Adding students to the record system
        recordSystem.addStudent(student1);
        recordSystem.addStudent(student2);
        recordSystem.addStudent(student3);


        //Displaying all student after addition
        System.out.println("Current students of the class: ");
        recordSystem.displayAllStudents();

        // Removing a student from the record system
        recordSystem.removeStudent(2);


        // Getting a student's details by their ID
        Student studentById = recordSystem.getStudent(1);
        if (studentById != null) {
            System.out.println("ID: " + studentById.getId());
            System.out.println("Name: " + studentById.getName());
            System.out.println("GPA: " + studentById.getGpa());
        } else {
            System.out.println("Student not found.");
        }

        // Displaying all students in the record system
        recordSystem.displayAllStudents();


    }
}
