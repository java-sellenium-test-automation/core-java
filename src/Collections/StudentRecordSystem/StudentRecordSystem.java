package Collections.StudentRecordSystem;

import java.util.HashMap;

public class StudentRecordSystem {
    private final HashMap<Integer,Student> studentHashMap;

    public  StudentRecordSystem(){
        studentHashMap = new HashMap<>();
    }

    public void addStudent(Student student){
        studentHashMap.put(student.getId(),student);
        System.out.println("Student "+student.getName()+" has joined the class");
    }

    public void removeStudent(int id) {
        studentHashMap.remove(id);
        System.out.println("Student with id: "+id+" has been removed from the class");
    }

    public Student getStudent(int id) {
        return studentHashMap.get(id);
    }

    public void displayAllStudents() {
        for (Student student : studentHashMap.values()) {
            System.out.println("\n");
            System.out.println("ID: " + student.getId());
            System.out.println("Name: " + student.getName());
            System.out.println("GPA: " + student.getGpa());
            System.out.println("-------------------- ");
        }

    }
}
