package Collections;

import java.util.ArrayList;
import java.util.List;

public class AverageOfEvenNumbers {

    public static void main(String[] args) {
        List<Integer> numbers = new ArrayList<>();
        numbers.add(5);
        numbers.add(10);
        numbers.add(3);
        numbers.add(8);
        numbers.add(1);

        int sum = 0;
        int counter = 0;

        for (int num:numbers) {
            if (num%2 == 0){
                sum += num;
                counter ++;
            }
        }

        System.out.println("Average of even numbers: " + sum/counter);



    }
}
