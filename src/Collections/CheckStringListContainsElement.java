package Collections;

import java.util.ArrayList;
import java.util.List;

public class CheckStringListContainsElement {
    public static void main(String[] args) {
        List<String> strings = new ArrayList<>();
        strings.add("apple");
        strings.add("banana");
        strings.add("orange");
        strings.add("coconut");

        String element = "banana";


        System.out.println("List contains element " + element);
    }

}
