import java.util.Scanner;
public class ClassWork1 {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        //Task 1
        System.out.println("What is your name: ");
        String name  = scanner.nextLine();
        System.out.println("Hello " + name );


        //Task 2
        System.out.println("Enter a number: ");
        int a = scanner.nextInt();

        if (a%2 == 0 && a%3 == 0){
            System.out.println("The number is divisable with both 2 and 3");
        }else {
            System.out.println("The number is NOT divisable with both 2 and 3");
        }

        //Task 3
        System.out.println("Enter first number: ");
        int b = scanner.nextInt();
        System.out.println("Enter second number: ");
        int c = scanner.nextInt();

        if (b==c){
            System.out.println("Numbers are equal");
        }else{
            System.out.println("Numbers are NOT equal");
        }


        //Task 4
        System.out.println("Enter a number: ");
        int d = scanner.nextInt();
        if (d==0){
            System.out.println("The number is equal to 0");
        }else if(d<0){
            System.out.println("The number is negative");
        }else {
            System.out.println("The number is positive");
        }


        //Task 5
        System.out.println("Enter a year: ");
        int year = scanner.nextInt();
        if (year%100 == 0 && year%400==0){
            System.out.println( year + " is leap year ");
        }else if (year%4 == 0){
            System.out.println( year + " is leap year ");
        }else{
            System.out.println( year + " is NOT leap year ");
        }

        //Task 6
        System.out.println("Enter temp in Celsius : ");
        int temp = scanner.nextInt();
        float convertedTemp = (temp * 9/5) + 32;
        System.out.println(temp + " Celsius is equal to " + convertedTemp +" Faranheit");

        //Task 7
        System.out.println("Enter first number: ");
        int num1 = scanner.nextInt();
        System.out.println("Enter second number: ");
        int num2 = scanner.nextInt();
        System.out.println("Enter third number: ");
        int num3 = scanner.nextInt();


        int max = num1;
        if (num2 > max) {
            max = num2;
        }
        if (num3 > max) {
            max = num3;
        }

        System.out.println("the maximum is " + max);

        //Task 8
        System.out.println("Enter size of side a: ");
        int sideA = scanner.nextInt();
        System.out.println("Enter size of side b: ");
        int sideB = scanner.nextInt();

        System.out.println("The area of rectangle is "+ (sideA * sideB));


        for (int i = 10; i < 20; i += 2) {
            if (i > 15)
                break;
            if (i % 4 == 0)
                continue;
            System.out.println (i);
        }


    }
}