package Book;

import java.util.ArrayList;
import java.util.Scanner;

public class BookInventory {

    private ArrayList<Book> books;

    public BookInventory(){books = new ArrayList<Book>();}


    public void addBook(){
        Scanner sc  =  new Scanner(System.in);
        System.out.println("Enter book title:");
        String title = sc.nextLine();
        System.out.println("Enter book author's name:");
        String author =  sc.nextLine();
        System.out.println("Enter publication year of the book");
        int publicationYear =  sc.nextInt();
        sc.nextLine();

        Book newBook = new Book(title,author,publicationYear);
        books.add(newBook);
        System.out.println("The new book has been successfully added to the inventory!!!");
    }

    public void removeBook(String title){
        for (Book book:books){
            if(book.getTitle().equals(title)){
                books.remove(book);
                System.out.println("The book with title "+title+" has been removed from inventory");
                return;
            }
        }
        System.out.println("The book with title "+title+" has been removed from inventory");
    };

    public void searchBook(String title){
        for(Book book:books){
            if(book.getTitle().equals(title)){
                System.out.println("The book has been found!");
                System.out.println("Book title is "+ book.getTitle());
                System.out.println("Book author is "+ book.getAuthor());
                System.out.println("Publication year is "+ book.getPublicationYear());
                return;
            }
        }
        System.out.println("The book with title "+title+" has been removed from inventory");
    }


    public void displayAllBooks(){
        if (books.isEmpty()){
            System.out.println("Book inventory is empty");
        }else{
            for(Book book:books){
                System.out.println("---------------------------------");
                System.out.println("Book title is "+ book.getTitle());
                System.out.println("Book author is "+ book.getAuthor());
                System.out.println("Publication year is "+ book.getPublicationYear());
            }
        }
    }

}
